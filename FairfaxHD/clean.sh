#!/usr/bin/env bash
rm -f *.sfd-* *Tmp* *_base.* FairfaxHD.ttf FairfaxHD.eot FairfaxHD.zip FairfaxPonaHD.* FairfaxHaxHD.* FairfaxSMHD.*
rm -rf fairfaxhd
